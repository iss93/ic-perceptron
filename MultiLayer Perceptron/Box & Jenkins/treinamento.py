"""Script referente a fase de treinamento da rede MultiLayer Perceptron
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import numpy as np


def tangente_hiperbolica(L):
    """Função que implementa a tangente hiperbólica"""
    return np.tanh(L)


def normalizacao(matriz):
    """Função para normalizar os dados"""
    return (matriz - np.min(matriz))/(np.max(matriz)-np.min(matriz))*2 + -1


"""Pré-processamento de dados"""


# Leitura do arquivo de treinamento
table = np.loadtxt("./files/dados_treinamento.txt")

# Normalização dos dados
normal = normalizacao(table)

# Separação de dados de entrada e de saída
entrada = normal[:, 0:2]
saida = normal[:, 2]

# Estiṕulação de valores
taxa_apr = 0.01     # Taxa de Aprendizado
epoca = 100        # Quantidade de épocas
num_epoca = 0       # Contador de épocas
precisao = 1e-12    # Precisão Requirida

# Criação da matriz de pesos de entrada de maneira aleatória
pesos_1 = np.random.uniform(0, 1, size=(3, 2))

# Criação do vetor de pesos de saida de maneira aleatória
pesos_2 = np.random.uniform(0, 1, size=3)

# Inserção do valor -1 na primeira coluna da matriz entrada
entrada = np.insert(entrada, 0, -1, axis=1)


"""Começo da Fase Forward"""


# Calcula L1 (somatoŕio das entradas * pesos entrada->intermediário)
L1 = np.dot(entrada, pesos_1)

# Calcula Y1 (Valor da camada intermediária)
Y1 = tangente_hiperbolica(L1)

# Inserção do valor -1 na primeira coluna da matriz Y1
Y1 = np.insert(Y1, 0, -1, axis=1)

# Calcula L2 (somatoŕio Y1 * pesos intermediário->saída)
L2 = np.dot(Y1, pesos_2)

# Calcula Y2 (Valor da camada de saída)
Y2 = tangente_hiperbolica(L2)
