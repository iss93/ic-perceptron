"""Script referente a fase de treinamento da rede Adaline
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import numpy as np


def treina():
    """Função que implementa o treinamento da rede"""

    # Leitura do arquivo de treinamento
    table = np.loadtxt("./files/xt.txt")

    # Separação de dados de entrada e de saída
    entrada = table[:, 0:3]
    saida = table[:, 3]

    # Estiṕulação de valores
    taxa_apr = 0.04     # Taxa de Aprendizado
    precisao = 1e-12    # Precisão
    limiar = 1          # Limiar
    epoca = 100         # Quantidade de épocas
    num_epoca = 0       # Contador de épocas
    eqm = 0             # Erro Quadrático Médio

    # Criação do vetor pesos de maneira aleatória
    pesos = np.random.uniform(0, 1, len(entrada[0]))

    # Inserção do limiar na primeira posição do vetor pesos
    pesos = np.insert(pesos, 0, limiar)

    print("\t\t\t%s FASE DE TREINAMENTO %s" % ('*' * 22, '*' * 22))
    print("\t\t\tPeso Inicial: ", pesos)

    # Inserção do valor -1 na primeira coluna da matriz entrada
    entrada = np.insert(entrada, 0, -1, axis=1)

    # Loop até atingir a precisão desejada ou até a quantidade máxima de épocas
    while True:
        # Eqm anterior recebe o valor atual do eqm
        eqm_anterior = eqm
        # Variável eqm recebe o novo valor de eqm
        eqm = erro_medio(entrada, saida, pesos)
        # Para todas as linhas da matriz entrada...
        for i in range(len(entrada)):
            # Zera a variável u (valor obtido)
            u = 0
            # Para todas as colunas de cada linha...
            for j in range(len(entrada[0])):
                # Incrementa u com a multiplicação de cada valor e seu peso
                u += entrada[i][j] * pesos[j]
            # Para todas as colunas de cada linha...
            for j in range(len(entrada[0])):
                # Modifica o valor do peso
                pesos[j] = pesos[j] + taxa_apr * (saida[i] - u) * entrada[i][j]
        # Incrementa o contador de épocas
        num_epoca += 1
        # Descoberta da variação do eqm ao fim de cada época
        diferenca = abs(eqm_anterior - eqm)
        # Caso a condição de parada seja atendida, saia do loop
        if num_epoca > epoca or diferenca <= precisao:
            break

    print("\t\t\tEpocas necessarias: ", num_epoca)
    print("\t\t\tPesos Finais: ", pesos)
    return pesos


def erro_medio(entrada, saida, pesos):
    """Função erro_medio que calcula o erro médio quadrático entre as saídas
       desejadas e obtidas"""

    eqm = 0
    count = 0
    # Para todas as linhas da matriz entrada...
    for i in range(len(entrada)):
        # Zera a variável u (valor obtido)
        u = 0
        # Para todas as colunas de cada linha...
        for j in range(len(entrada[0])):
            # Incrementa u com a multiplicação de cada valor e seu peso
            u += entrada[i][j] * pesos[j]
        # Incrementa o valor do eqm
        eqm += (saida[i] - u) ** 2
        # Incrementa o contador
        count += 1
    # Realiza a média
    eqm_total = eqm/count

    return eqm_total
