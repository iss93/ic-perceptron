"""Script referente a 'main' da rede Adaline
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import matplotlib.pyplot as plt
import numpy as np
from operacao import testa
from treinamento import treina

# Executa o Adaline
treino = treina()
resultados = testa(treino)

# Separa resultados obtidos e desejados
obtido = resultados[0]
desejado = resultados[1]

# Calcula o EQM
resultado = (desejado-obtido)**2
eqm = np.mean(resultado)

print("\n\t\t\tEQM", eqm)

# Gera gráfico da função matemática
x = np.arange(0, 2*np.pi, 0.05)
# y = -np.pi + 0.565 * np.sin(x) + 2.657 * np.cos(x) + 0.674 * x
y = desejado


fig = plt.figure(1)
ax1 = fig.add_subplot(121)
ax1.plot(x, y)
ax1.grid(True)
ax1.set_ylim((-4, 4))
ax1.set_title('Função Matemática')

for label in ax1.get_xticklabels():
    label.set_color('r')

# Gera gráfico da função aproximada
x = np.arange(0, 2*np.pi, 0.05)
y = obtido

ax2 = fig.add_subplot(122)
ax2.plot(x, y)
ax2.grid(True)
ax2.set_ylim((-4, 4))
ax2.set_title('Função Aproximada')

for label in ax2.get_xticklabels():
    label.set_color('g')

plt.show()
