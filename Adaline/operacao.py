"""Script referente a fase de operação da rede Adaline
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import numpy as np


def testa(pesos):
    """Função que implementa a classificação das amostras utilizando os pesos
       provenientes da fase de treinamento"""

    # Leitura do arquivo de teste
    dados = np.loadtxt("./files/xv.txt")

    # Separação de dados de entrada e de saída
    entrada = dados[:, 0:3]
    saida = dados[:, 3]

    # Inserção do valor -1 na primeira coluna da matriz entrada
    entrada = np.insert(entrada, 0, -1, axis=1)

    y = []

    print("\n\t\t\t%s FASE DE OPERACAO %s" % ('*' * 23, '*' * 23))

    # Para todas as linhas da tabela amostras...
    for i in range(len(entrada)):
        # Zera a variável u (valor obtido)
        u = 0
        # Para cada amostra...
        for j in range(len(entrada[i])):
            # Incrementa u com a multiplicação de cada valor e seu peso
            u += entrada[i][j] * pesos[j]
        print("\t\t\tSaída desejada: %s\tSaída obtida: %s" % (saida[i], u))
        # Adiciona o valor u no vetor y
        y.append(u)

    # Armazena as saídas obtida e desejada em um vetor
    resultados = []
    resultados.append(y)
    resultados.append(saida)

    return resultados
