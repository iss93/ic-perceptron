#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 17 10:31:10 2017

@author: igor
"""

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


dataset = np.loadtxt("seeds_dataset.txt", delimiter="\t")
X, Y = dataset[:, :-1], dataset[:, -1]

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = 0.2, random_state = 0)


#sc_X = StandardScaler()
#X_train = sc_X.fit_transform(X_train)
#X_test = sc_X.transform(X_test)
#sc_y = StandardScaler()

Y_s_train = []
Y_s_test = []
Y_s_pred = []
Y_s_prob = []

for x in range(1, 4):
    
    Y1_train = [int(y == x) for y in Y_train]
#    Y1_test = [int(y == x) for y in Y_test]
    
    logistic = LogisticRegression()    
    logistic.fit(X_train, Y1_train)
    
    Y_pred = logistic.predict(X_test)
    Y_prob = logistic.predict_proba(X_test)
    
    Y_s_pred.append(Y_pred)
    Y_s_prob.append(Y_prob)

z = []
for x in range(0, 3):
    y = Y_s_prob[x][:, 1]
    z.append(y)

teste = []
for x in range(len(z[0])):
    a = z[0][x]
    b = z[1][x]
    c = z[2][x]
    maior = 1 if (z[0][x] > z[1][x] and z[0][x] > z[2][x]) else 2 if (z[1][x] > z[2][x] and z[1][x] > z[0][x]) else 3
    teste.append(maior)
z.append(teste)

count = 0
for x in range(len(Y_test)): 
    if teste[x] == Y_test[x]:
        count += 1
porcentagem = count / len(Y_test)

print(porcentagem)        