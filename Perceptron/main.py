"""Script referente a 'main' da rede Perceptron
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


from treinamento import treina
from operacao import testa

# Executa o Perceptron
pesos = treina()
testa(pesos)
