"""Script referente a fase de operação da rede Perceptron
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import numpy as np


def testa(pesos):
    """Função que implementa a classificação das amostras utilizando os pesos
       provenientes da fase de treinamento"""

    # Leitura do arquivo de teste
    table = np.loadtxt("./files/dados_validacao.txt")

    # Inserção do valor -1 na primeira coluna da matriz das amostras
    amostras = np.insert(table, 0, -1, axis=1)

    print("\n\t\t\t%s FASE DE OPERACAO %s" % ('*' * 23, '*' * 23))

    # Para todas as linhas da tabela amostras...
    for i in range(len(amostras)):
        # Zera a variável u (valor obtido)
        u = 0
        # Para cada amostra...
        for j in range(len(amostras[0])):
            # Incrementa u com a multiplicação de cada valor e seu peso
            u += amostras[i][j] * pesos[j]
        # Variável y recebe o resultado da função sinal
        y = sinal(u)

        # Caso y seja igual a -1...
        if y == -1:
            print("\t\t\t    Amostra %s pertence a classe P1" % (table[i]))
        # Caso contrário...
        else:
            print("\t\t\t    Amostra %s pertence a classe P2" % (table[i]))


def sinal(u):
    """Função sinal que retorna 1 caso o valor obtido seja maior ou igual a 0
       e retorna -1 caso contrário"""
    return 1 if u >= 0 else -1
