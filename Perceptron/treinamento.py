"""Script referente a fase de treinamento da rede Perceptron
   Autores: Igor Silva, Evandro Almeida, João Carlos
   10º Período Engenharia de Computação - 2/2017
   Disciplina: Inteligência Computacional
   Professor: Maurílio José Inácio
   Faculdade de Ciência e Tecnologia de Montes Claros - FACIT"""


import numpy as np


def treina():
    """Função que implementa o treinamento da rede"""

    # Leitura do arquivo de treinamento
    table = np.loadtxt("./files/dados_treinamento.txt")

    # Separação de dados de entrada e de saída
    entrada = table[:, 0:3]
    saida = table[:, 3]

    # Estiṕulação de valores
    taxa_apr = 0.01     # Taxa de Aprendizado
    limiar = 0.1        # Limiar
    epoca = 1000        # Quantidade de épocas
    num_epoca = 0       # Contador de épocas

    # Criação do vetor pesos de maneira aleatória
    pesos = np.random.uniform(-1, 1, len(entrada[0]))

    # Inserção do limiar na primeira posição do vetor pesos
    pesos = np.insert(pesos, 0, limiar)

    print("\t\t\t%s FASE DE TREINAMENTO %s" % ('*' * 22, '*' * 22))
    print("\t\t\tPeso Inicial: ", pesos)

    # Inserção do valor -1 na primeira coluna da matriz entrada
    entrada = np.insert(entrada, 0, -1, axis=1)

    # Loop até não ocorrer erros ou até a quantidade máxima de épocas
    while True:
        # Seta a variável erro como False
        erro = False
        # Para todas as linhas da matriz entrada...
        for i in range(len(entrada)):
            # Zera a variável u (valor obtido)
            u = 0
            # Para todas as colunas de cada linha...
            for j in range(len(entrada[0])):
                # Incrementa u com a multiplicação de cada valor e seu peso
                u += entrada[i][j] * pesos[j]
            # Variável y recebe o resultado da função sinal
            y = sinal(u)
            # Caso y seja diferente da saída desejada...
            if (y != saida[i]):
                # Para todas as colunas desta linha...
                for j in range(len(entrada[0])):
                    # Modifica o valor do peso
                    erro_aux = saida[i] - y
                    pesos[j] = pesos[j] + taxa_apr * erro_aux * entrada[i][j]
                # Altera o status da variável erro
                erro = True
        # Incrementa o contador de épocas
        num_epoca += 1
        # Caso a condição de parada seja atendida, saia do loop
        if num_epoca > epoca or not erro:
            break

    print("\t\t\tEpocas necessarias: ", num_epoca)
    print("\t\t\tPesos Finais: ", pesos)
    return pesos


def sinal(u):
    """Função sinal que retorna 1 caso o valor obtido seja maior ou igual a 0
       e retorna -1 caso contrário"""
    return 1 if u >= 0 else -1
